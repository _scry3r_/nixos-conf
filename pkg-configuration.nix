{ config, pkgs, lib, ... }:

{
  environment.systemPackages = with pkgs; [
    vim emacs wget curl git lynx ranger nmap rxvt_unicode gnupg awscli virtualbox nginx python3 meld mercurial vscode docker wireshark
  ];

  fonts.fonts = with pkgs; [
    hermit
    source-code-pro
    terminus_font
  ];
  
  environment.variables.EDITOR = "urxvt";

}
